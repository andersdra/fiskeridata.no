#!/bin/bash
# fetch and process text weather report for the next 3 days from yr.no
# run 00 every hour '0 * * * * /path/of/script'

hour_now="$(date +%H)"

if python3 -c "check_hours=['00', '07', '08', '09', '10', '11', '12', '13', '14']
if '$hour_now' in check_hours:
    exit(0)
else:
    exit(1)"
then
# get text then strip html tags + whitespace
  text_report="$(curl https://www.yr.no/tekstvarsel/ 2>/dev/null | \
    grep 'Møre og Romsdal' | \
    sed -e 's/<[^>]*>//g' | \
    sed -e 's/^[ \t]*//')"
# split reports
  today="$(echo "$text_report" | cut -d $'\n' -f1)"
  tomorrow="$(echo "$text_report" | cut -d $'\n' -f2)"
  dayafter="$(echo "$text_report" | cut -d $'\n' -f3)"
# save at path accessible by webserver
  if [ -n "$today" ];then echo "$today" > /var/www/html/vaer/varsel_idag.txt;fi
  if [ -n "$tomorrow" ];then echo "$tomorrow" > /var/www/html/vaer/varsel_imorgen.txt;fi
  if [ -n "$dayafter" ];then echo "$dayafter" > /var/www/html/vaer/varsel_overimorgen.txt;fi
  exit 0
else
  exit 1
fi
