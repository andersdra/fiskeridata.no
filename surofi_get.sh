#!/bin/bash

if [ -z "$1" ];
then
    echo "example: sh get_surofi.sh page from to"
    echo "from/to: dd.mm.yyyy"
    exit
fi;

get_surofi()
{
   page=$(curl --silent --show-error --data "side=$1&from=$from_date&to=$to_date" 'https://surofi.no/salgs-og-landingsinfo/landingsinfo/' | \
       grep -o '<td.*td>' | sed 's/<[^>]*>//g')
   echo "$page" | head '-n-1' - > './tmp/cleaned'
   num_pages=$(echo "$page" | tail -1 | awk "{print $4}" | rev | cut -d' ' -f 1 | rev)
   python3 surofi_parse.py < './tmp/cleaned'
   if [ -z "num_pages" ]
     then
       exit 0
   else
     if [ "$1" -lt "$num_pages" ];
       then
         sleep .1
         get_surofi $(($1+1))
     fi
   fi
}

cur_page="$1"
from_date="$2"
to_date="$3"

if [ -n "$from_date" ] && [ -n "$from_date" ]
  then
    get_surofi "$cur_page"
fi
