#!/bin/sh
# fetches fish delivery data from rafisklaget.no 'X 0001XX'/'XX0001XX'

verification_url=\
'https://www.rafisklaget.no/portal/pls/portal/STAT.NRWEB.LANDINGSSKJEMA'
delivery_url=\
'https://www.rafisklaget.no/portal/pls/portal/!STAT.NRWEB.LANDINGSOPPLYSNING'

verification=\
$(curl --silent --cookie-jar "$1".txt "$verification_url" \
| grep --only-matching '<b.*b>' - | sed 's/<[^>]*>//g' \
| awk '{print $1 "+" $3}' | bc)

sleep .5

# get deliveries
curl --silent --cookie "$1".txt --data "regmerke=$1&svar=$verification" \
"$delivery_url" > "$1_delivery"

rm "$1".txt

python3 rafisk_cleanup.py < "$1_delivery"

