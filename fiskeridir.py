#!/usr/bin/python3
# -*- coding: utf-8 -*-
import subprocess
import requests
from bs4 import BeautifulSoup
geturl = "https://register.fiskeridir.no/fartoyreg/?m=frty&s=1"

def list_boat_info(boat):
    s = requests.Session()
    payload = {"regmrk" : boat, "rkall": "", "navn": "", "submit" : "Vis+rapport"}
    page = s.post(geturl, data=payload) # + boat
    soup = BeautifulSoup(page.content, 'lxml')
    boatinfo = soup.find_all('table')
    for a in range(1, len(boatinfo)):
        print("A: {} \n".format(a))
        print(boatinfo[a].text)


def update_boatlist():
    s = requests.Session()
    data = s.get(geturl)
    data = BeautifulSoup(data.content, 'lxml')
    data = data.find_all("table")
    data = data[1]
    with open("../fiskeridir/boat_list.htm", "w") as f:
        f.write(str(data) + '\n')


def find_boat(searchword, ebid=0):
    while True:
        try:
            if len(searchword) < 2:
                return "Skriv minst to bokstaver/tall"
            a = subprocess.check_output(["grep", searchword.upper(), "../fiskeridir/boat_list.htm",]).decode('utf-8')
            break
        except subprocess.CalledProcessError:
            return "Fant ingen båter ved søk på %s" % searchword
            break
    b = BeautifulSoup(a, 'lxml')
    c = b.find_all('td')  # row
    d = b.find_all('a')  # link
    c[0] = d[0].get('href')
    for a in range(1, len(c)):
        c[a] = c[a].text
    if ebid:
        return c[0]
    else:
        return c[2]


if __name__ == "__main__":
    a = list_boat_info(find_boat(""))
