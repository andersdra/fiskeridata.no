#!/bin/python3

import html.parser
import sys
import io

def parse_surofi(surofi_raw):
    counter = 0
    for line in surofi_raw:
        print('linje: ', line)
        if counter <= 8:
            if counter == 8:
                sys.stdout.write(html.parser.unescape(line))
                sys.stdout.flush()
                counter = 0
            else:
                sys.stdout.write(html.parser.unescape(line).strip() + ', ')
                counter += 1
    return 0


if __name__ == "__main__":
    parse_surofi(io.TextIOWrapper(sys.stdin.buffer, encoding='iso-8859-1'))

