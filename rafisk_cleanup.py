#!/usr/bin/python3
import io
import sys
from bs4 import BeautifulSoup

def parse_deliveries(inputs):
    soup = BeautifulSoup(inputs, "lxml")
    boat = soup.find('span').text
    deliveries = soup.find_all('td')
    if 'ingen' in boat:
        print("NO_DELIVERIES")
        return 1
    final_list = [boat]
    tmp_list = []
    last_buyer = ''
    buyer = ''
    cur_date = ''
    last_date = ''
    total_kg = 0
    delivery_kg = 0
    for entry in deliveries:
        if entry.has_attr('class'): # weight entry, last on line
            cur_buyer = tmp_list[1]
            cur_date  = tmp_list[0]
            if total_kg == 0:
                last_buyer = cur_buyer
            kg_delivered = int(entry.text.replace('.', ''))
            tmp_list.append(str(kg_delivered))
            total_kg += kg_delivered
            delivery_kg += kg_delivered
            final_list.append(tmp_list)
            if last_buyer == '':
                if cur_buyer:
                    buyer = cur_buyer
                    last_buyer = cur_buyer
                    sys.stdout.write("Landing: {} KG\n".format(delivery_kg))
                    delivery_kg = 0
            elif last_buyer:
                if cur_buyer == '':
                    last_buyer = ''
            print(tmp_list)
            if entry == deliveries[-1]: # last entry, does not get caught by last/cur_buyer
                sys.stdout.write("Landing: {} KG\n".format(delivery_kg))
            tmp_list = []
        else:
            tmp_list.append(entry.text)
    sys.stdout.write("Landet i periode: {} KG\n".format(total_kg))
    sys.stdout.flush()

if __name__ == "__main__":
    parse_deliveries(io.TextIOWrapper(sys.stdin.buffer, encoding='iso-8859-1'))
